FROM node:alpine AS builder

RUN mkdir -p /usr/src/homepage/ 
WORKDIR /usr/src/homepage
COPY . .

RUN npm install
RUN $(npm bin)/ng build --prod

FROM nginx:alpine
COPY --from=builder /usr/src/homepage/dist/homepage /usr/share/nginx/html
